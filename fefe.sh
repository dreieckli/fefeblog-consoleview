#!/bin/bash

_version='20221016.03'
_fefeurl='http://blog.fefe.de/'

### COPYING: This software is licensed under the GNU General Public License, version 3.0.
#
#     fefe.sh -- simple shell script to view http://blog.fefe.de on the console.
#     Copyright (C) 2022-... dreieck (at) kettenbruch ]dot] de
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###

_datetime="$(date +%Y-%m-%d_%H-%M-%S)"

stdout()
# To write to stdout.
# Options: '-n': Without trailing newline.
{
  if [ "$1" == "-n" ]; then
    _fmt='%s'
    shift
  else
    _fmt='%s\n'
  fi

  if [ $# -gt 1 ]; then
    errmsg "BUG: Too many arguments passed to internal function 'stdout()'".
    return 12
  else
    printf "${_fmt}" "$1"
  fi
}

stderr()
# To write to stderr.
{
  stdout "$@" > /dev/stderr
}

msg()
# To print messages for the user.
{
  stdout "$@"
}

stdout_prefix()
{
  prefix="$1"
  while read line; do
    stdout "${prefix}${line}"
  done
}

verbose() {
  if "${_verbose}"; then
    msg "$@"
  fi
}

debug() {
  if "${_debug}"; then
    msg "$@"
  fi
}

errmsg() {
  stderr "$@"
}

exiterror() {
  if [ $# -ge 2 ]; then
    _exitcode="$2"
  else
    _exitcode=1
  fi
  if [ $# -ge 1 ]; then
    _msg="$1"
  else
    _msg="$0: Unspecified Error. Aborting."
  fi
  errmsg "${_msg}"
  exit "${_exitcode}"
}

printusage() {
  stdout "Usage:"
  stdout "  $0 [arguments ...]"
  stdout ""
  stdout "Arguments (all optional):"
  stdout "  -h       | --help | help   Print this message and exit."
  stdout "  -V       | --version       Print version number and exit. (Version is: '${_version}'.)"
  stdout "  -U <url> | --url <url>     Set the base URL we retrieve information from. Default is '${_fefeurl}'."
  stdout ""
  stdout "(Info: We were started at ${_datetime}.)"
}

while [ $# -ge 1 ]; do
  case "$1" in
    "-h"|"--help"|"help")
      printusage
      exit 0
      shift
    ;;
    "-V"|"--version")
      msg "${_version}"
      exit 0
      shift
    ;;
    "-U"|"--url")
      if [ $# -ge 2 ]; then
        _fefeurl="$2"
        shift
        shift
      else
        errmsg "$0: Error: Argument '$1' needs a parameter, but none was specified."
        errmsg "Run '$0 -h' for help."
        exiterror "Aborting." 2
        shift
      fi
    ;;
    *)
      _unknownarg="$1"
      shift
      errmsg "$0: Error: Unrecognised argument '${_unknownarg}'."
      errmsg ""
      errmsg "$(printusage)"
      errmsg ""
      exiterror "Aborting." 1
    ;;
  esac
done


if _cols="`tput cols 2>/dev/null`" && [ "${_cols}" -ge 10 ]; then
  _pandoc_wrap_options=(
    '--wrap=auto'
    "--columns=${_cols}"
  )
else
  _pandoc_wrap_options=(
    '--wrap=preserve'
  )
fi


curl --silent --location "${_fefeurl}" | pandoc -f html -t markua "${_pandoc_wrap_options[@]}" | less
