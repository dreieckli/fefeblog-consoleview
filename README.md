# fefeblog-consoleview

A very simble bash script to view "[Fefes Blog](http://blog.fefe.de/)" on the console.


## Dependencies

On Arch Linux, this script depends on the following packages (in brackets the bare commands it uses):

* **bash**:    For running this script. (`bash`)
* **curl**:    To download the website. (`curl`)
* **less**:    For paging the output. (`less`)
* **ncurses**: To get the number of columns in the current terminal. (`tput`)
* **pandoc**:  To convert the fetched HTML to text/ markdown for display. (`pandoc`)


## Usage

Output from `./fefe.sh -h`, version 20221016.03 (which is the up to date version as of 2022-10-16, 13:08:13 UTC):

```
Usage:
  ./fefe.sh [arguments ...]

Arguments (all optional):
  -h       | --help | help   Print this message and exit.
  -V       | --version       Print version number and exit. (Version is: '20221016.03'.)
  -U <url> | --url <url>     Set the base URL we retrieve information from. Default is 'http://blog.fefe.de/'.

(Info: We were started at 2022-10-16_15-08-13.)
```


## License

This software is licensed under the GNU General Public License (GPL), version 3.0. See the file [`COPYING.GPL3.txt`](./COPYING.GPL3.txt) within the repository for the license text.
